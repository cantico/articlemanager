<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2018 by CANTICO ({@link http://www.cantico.fr})
 */


$addon_articlemanager = bab_getAddonInfosInstance('articlemanager');

if (!$addon_articlemanager) {
    return;
}

define('FUNC_ARTICLEMANAGER_PHP_PATH', $addon_articlemanager->getPhpPath());
define('FUNC_ARTICLEMANAGER_SET_PATH', FUNC_ARTICLEMANAGER_PHP_PATH);
define('FUNC_ARTICLEMANAGER_UI_PATH', FUNC_ARTICLEMANAGER_PHP_PATH);
define('FUNC_ARTICLEMANAGER_PORTLETS_PATH', FUNC_ARTICLEMANAGER_PHP_PATH . 'portlets/');
bab_functionality::includeFile('App');



/**
 * Func_App_Articlemanager
 */
class Func_App_Articlemanager extends Func_App
{

    public function __construct()
    {
        parent::__construct();

        $this->addonPrefix = 'articlemanager';
        $this->addonName = 'articlemanager';
        $this->classPrefix = $this->addonPrefix . '_';
        $this->controllerTg = 'addon/articlemanager/main';
    }


    /**
     * @return string
     *
     */
    public function getDescription()
    {
        return 'Article manager';
    }




    /**
     * Includes ArticleSet class definition.
     */
    public function includeArticleSet()
    {
        require_once FUNC_ARTICLEMANAGER_SET_PATH . 'article.class.php';
    }

    /**
     * @return string
     */
    public function ArticleClassName()
    {
        return $this->classPrefix . 'Article';
    }

    /**
     * @return string
     */
    public function ArticleSetClassName()
    {
        return $this->ArticleClassName() . 'Set';
    }


    /**
     * @return articlemanager_ArticleSet
     */
    public function ArticleSet()
    {
        $this->includeArticleSet();
        $className = $this->ArticleSetClassName();
        $set = new $className($this);
        return $set;
    }





    function setTranslateLanguage($language)
    {
        parent::setTranslateLanguage($language);
        $translate = bab_functionality::get('Translate/Gettext');
        /* @var $translate Func_Translate_Gettext */
        $translate->setLanguage($language);
    }


    /**
     * Translates the string.
     *
     * @param string $str
     * @return string
     */
    function translate($str, $str_plurals = null, $number = null)
    {
        require_once FUNC_ARTICLEMANAGER_PHP_PATH . 'functions.php';
        $translation = $str;
        if ($translate = bab_functionality::get('Translate/Gettext')) {
            /* @var $translate Func_Translate_Gettext */
            $translate->setAddonName('articlemanager');
            $translation = $translate->translate($str, $str_plurals, $number);
        }
        if ($translation === $str) {
            $translation = parent::translate($str, $str_plurals, $number);
        }

        return $translation;
    }


    /**
     * Includes Controller class definition.
     */
    public function includeController()
    {
        parent::includeController();
        require_once FUNC_ARTICLEMANAGER_PHP_PATH . 'controller.class.php';
    }

    /**
     *
     * @return articlemanager_Controller
     */
    function Controller()
    {
        self::includeController();
        return new articlemanager_Controller($this);
    }


    /**
     * Include class articlemanager_Access
     */
    protected function includeAccess()
    {
        parent::includeAccess();
        require_once FUNC_ARTICLEMANAGER_PHP_PATH . 'access.class.php';
    }



    /**
     * Include class articlemanager_Ui
     */
    public function includeUi()
    {
        parent::includeUi();
        require_once FUNC_ARTICLEMANAGER_PHP_PATH . 'ui.class.php';
    }

    /**
     * The articlemanager_Ui object propose an access to all ui files and ui objects (widgets)
     *
     * @return articlemanager_Ui
     */
    public function Ui()
    {
        $this->includeUi();
        return bab_getInstance('articlemanager_Ui');
    }


    /**
     * Get upload path
     * if the method return null, no upload functionality
     *
     * @return bab_Path
     */
    public function uploadPath()
    {
        require_once $GLOBALS['babInstallPath'].'utilit/path.class.php';
        return new bab_Path(bab_getAddonInfosInstance('articlemanager')->getUploadPath());
    }


    /**
     * Include class app_Portlet
     *
     */
    protected function includePortlet()
    {
        parent::includePortlet();
        require_once FUNC_ARTICLEMANAGER_PORTLETS_PATH . '/portlet.class.php';
    }



    /**
     * The app_Portlet object propose an access to all portlets
     *
     * @return articlemanager_Portlet
     */
    public function Portlet()
    {
        $this->includePortlet();
        $portlet = bab_getInstance('articlemanager_Portlet')->setApp($this);
        $portlet->includeBase();
        return $portlet;
    }


    /**
     * Returns the configuration for the specified path key
     *
     * @param string    $path           The full path with the key
     * @param mixed     $defaultValue
     * @return mixed
     */
    public function getConf($path, $defaultValue = null)
    {
        static $registry = null;
        if (!isset($registry)) {
            $registry = bab_getRegistry();
        }

        if (substr($path, 0, 1) !== '/') {
            $path = '/' . $path;
        }
        $path = '/' . $this->addonPrefix . $path;

        if (defined($path)) {
            return constant($path);
        }

        $elements = explode('/', $path);
        $key = array_pop($elements);
        $path = implode('/', $elements);
        $registry->changeDirectory($path);
        $value = $registry->getValue($key);

        if (!isset($value)) {
            $value = $defaultValue;
        }

        return $value;
    }
    
    /**
     * Includes TopicSet class definition.
     */
    public function includeTopicSet()
    {
        require_once FUNC_ARTICLEMANAGER_SET_PATH . 'topic.class.php';
    }
    
    /**
     * @return string
     */
    public function TopicClassName()
    {
        return $this->classPrefix . 'Topic';
    }
    
    /**
     * @return string
     */
    public function TopicSetClassName()
    {
        return $this->TopicClassName() . 'Set';
    }
    
    
    /**
     * @return articlemanager_TopicSet
     */
    public function TopicSet()
    {
        $this->includeTopicSet();
        $className = $this->TopicSetClassName();
        $set = new $className($this);
        return $set;
    }
    
    /**
     * Includes TopicCategorySet class definition.
     */
    public function includeTopicCategorySet()
    {
        require_once FUNC_ARTICLEMANAGER_SET_PATH . 'topiccategory.class.php';
    }
    
    /**
     * @return string
     */
    public function TopicCategoryClassName()
    {
        return $this->classPrefix . 'TopicCategory';
    }
    
    /**
     * @return string
     */
    public function TopicCategorySetClassName()
    {
        return $this->TopicCategoryClassName() . 'Set';
    }
    
    
    /**
     * @return articlemanager_TopicCategorySet
     */
    public function TopicCategorySet()
    {
        $this->includeTopicCategorySet();
        $className = $this->TopicCategorySetClassName();
        $set = new $className($this);
        return $set;
    }
}
