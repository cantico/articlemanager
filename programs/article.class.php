<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2018 by CANTICO ({@link http://www.cantico.fr})
 */


require_once dirname(__FILE__) . '/functions.php';

$App = articlemanager_App();

$App->includeTraceableRecordSet();


/**
 *
  PRIMARY KEY  (id)
  UNIQUE KEY uuid (uuid)
 * @method articlemanager_Article get()
 * @method articlemanager_Article newRecord()
 * @method articlemanager_Article[] select()
 * 
 * @param   ORM_IntField            $id
 * @param   ORM_DatetimeField       $date
 * @param   ORM_DatetimeField       $date_publication
 * @param   ORM_DatetimeField       $date_archiving
 * @param   ORM_DatetimeField       $date_modification
 * @param   ORM_StringField         $title
 * @param   ORM_TextField           $head
 * @param   ORM_StringField         $head_format
 * @param   ORM_TextField           $body
 * @param   ORM_StringField         $body_format
 * @param   ORM_StringField         $page_title
 * @param   ORM_StringField         $page_keywords
 * @param   ORM_TextField           $page_description
 * @param   ORM_StringField         $rewritename
 * @param   ORM_StringField         $lang
 * @param   ORM_UserField           $id_author
 * @param   ORM_UserField           $id_modifiedby
 * @param   ORM_StringField         $uuid
 * @param   ORM_StringField         $restriction
 * @param   ORM_IntField            $ordering
 * @param   ORM_IntField            $index_status
 * @param   ORM_StringField         $archive
 * @param   articlemanager_Topic    $id_topic
 */
class articlemanager_ArticleSet extends app_RecordSet
{
    /**
     * @param Func_App $App
     */
    public function __construct(Func_App_Articlemanager $App = null)
    {
        parent::__construct($App);
        $this->setTableName(BAB_ARTICLES_TBL);
        $App = $this->App();

        $this->setDescription($App->translatable('Article'));

        $this->setPrimaryKey('id');

        $this->addFields(
            ORM_DatetimeField('date')
                ->setDescription($App->translatable('Date')),
            ORM_DatetimeField('date_publication')
                ->setDescription($App->translatable('Publication date')),
            ORM_DatetimeField('date_archiving')
                ->setDescription($App->translatable('Archiving date')),
            ORM_DatetimeField('date_modification')
                ->setDescription($App->translatable('Modification date')),
            ORM_StringField('title')
                ->setDescription($App->translatable('Title')),
            ORM_TextField('head')
                ->setDescription($App->translatable('Head')),
            ORM_StringField('head_format'),
            ORM_TextField('body')
                ->setDescription($App->translatable('Body')),
            ORM_StringField('body_format'),
            ORM_StringField('page_title'),
            ORM_StringField('page_keywords'),
            ORM_TextField('page_description'),
            ORM_StringField('rewritename'),
            ORM_StringField('lang'),
            ORM_UserField('id_author')
                ->setDescription($App->translatable('Author')),
            ORM_UserField('id_modifiedby'),
            ORM_StringField('uuid', 36),
            ORM_StringField('restriction'),
            ORM_IntField('ordering'),
            ORM_IntField('index_status'),
            ORM_StringField('archive', 1)
                ->setDescription($App->translatable('Archived'))
        );

       $this->hasOne('id_topic', $App->TopicSetClassName());
    }

    /**
     * {@inheritDoc}
     * @see app_RecordSet::isCreatable()
     */
    public function isCreatable()
    {
        return true;
    }


    /**
     * @return ORM_Criteria
     */
    public function isReadable()
    {
        return $this->all();
    }


    /**
     * @return ORM_Criteria
     */
    public function isUpdatable()
    {
        return $this->all();
    }

    /**
     * @return ORM_Criteria
     */
    public function isDeletable()
    {
        return $this->isUpdatable();
    }
}




/**
 * @param   int                     $id
 * @param   datetime                $date
 * @param   datetime                $date_publication
 * @param   datetime                $date_archiving
 * @param   datetime                $date_modification
 * @param   string                  $title
 * @param   string                  $head
 * @param   string                  $head_format
 * @param   string                  $body
 * @param   string                  $body_format
 * @param   string                  $page_title
 * @param   string                  $page_keywords
 * @param   string                  $page_description
 * @param   string                  $rewritename
 * @param   string                  $lang
 * @param   int                     $id_author
 * @param   int                     $id_modifiedby
 * @param   string                  $uuid
 * @param   string                  $restriction
 * @param   int                     $ordering
 * @param   int                     $index_status
 * @param   string                  $archive
 * @param   articlemanager_Topic    $id_topic
 */
class articlemanager_Article extends app_TraceableRecord
{
}
