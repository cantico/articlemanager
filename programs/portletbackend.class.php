<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2018 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__) . '/functions.php';

bab_Functionality::includefile('PortletBackend');



$GLOBALS['Func_PortletBackend_Articlemanager_Categories'] = array(
    'portal_tools' => articlemanager_translate('Portal tools')
);


/**
 * Crm Portlet backend
 */
class Func_PortletBackend_Articlemanager extends Func_PortletBackend
{

    /**
     * @return string
     */
    public function getDescription()
    {
        return articlemanager_translate('Articlemanager');
    }


    /**
     * @param string $category
     * @return articlemanager_PortletDefinition_Articlemanager[]
     */
    public function select($category = null)
    {
        $addon = bab_getAddonInfosInstance('articlemanager');
        if (!$addon || !$addon->isAccessValid()) {
            return array();
        }

        global $Func_PortletBackend_Articlemanager_Categories;
        if (empty($category) || in_array($category, $Func_PortletBackend_Articlemanager_Categories)) {
            return array(
                'Articlemanager' => $this->portlet_Articlemanager(),
            );
        }

        return array();
    }


    /**
     * @return articlemanager_PortletDefinition_Articlemanager
     */
    public function portlet_Articlemanager()
    {
        return new articlemanager_PortletDefinition_Articlemanager();
    }


    /**
     * get a list of categories supported by the backend
     * @return Array
     */
    public function getCategories()
    {
        global $Func_PortletBackend_Articlemanager_Categories;

        return $Func_PortletBackend_Articlemanager_Categories;
    }

    /**
     * (non-PHPdoc)
     * @see Func_PortletBackend::getConfigurationActions()
     */
    public function getConfigurationActions()
    {
        return array();
    }
}







////////////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////////////////////////////


class articlemanager_PortletDefinition_Articlemanager implements portlet_PortletDefinitionInterface
{
    /**
     * @var bab_addonInfos $addon
     */
    protected $addon;

    /**
     *
     */
    public function __construct()
    {
        $this->addon = bab_getAddonInfosInstance('articlemanager');
    }


    /**
     * {@inheritDoc}
     * @see portlet_PortletDefinitionInterface::getId()
     */
    public function getId()
    {
        return 'Articlemanager';
    }


    /**
     * {@inheritDoc}
     * @see portlet_PortletDefinitionInterface::getName()
     */
    public function getName()
    {
        return articlemanager_translate('Articlemanager');
    }


    /**
     * {@inheritDoc}
     * @see portlet_PortletDefinitionInterface::getDescription()
     */
    public function getDescription()
    {
        return articlemanager_translate('Configurable article view.');
    }


    /**
     * {@inheritDoc}
     * @see portlet_PortletDefinitionInterface::getPortlet()
     */
    public function getPortlet()
    {
        return new articlemanager_Portlet_Articlemanager();
    }


    /**
     * @return array
     */
    public function getPreferenceFields()
    {
        return array(
            array(
                'type' => 'list',
                'name' => 'displayType',
                'label' => articlemanager_translate('Display type'),
                'options' => array(
                    array(
                        'label' => articlemanager_translate('List'),
                        'value' => 'list'
                    ),
                )
            ),
        );
    }


    /**
     *
     * {@inheritDoc}
     * @see portlet_PortletDefinitionInterface::getRichIcon()
     */
    public function getRichIcon()
    {
        return $this->addon->getImagesPath() . 'articlemanager128.png';
    }


    /**
     * {@inheritDoc}
     * @see portlet_PortletDefinitionInterface::getIcon()
     */
    public function getIcon()
    {
        return $this->addon->getImagesPath() . 'articlemanager48.png';
    }


    /**
     * {@inheritDoc}
     * @see portlet_PortletDefinitionInterface::getThumbnail()
     */
    public function getThumbnail()
    {
        return $this->addon->getImagesPath() . 'thumbnail.png';
    }


    /**
     * {@inheritDoc}
     * @see portlet_PortletDefinitionInterface::getConfigurationActions()
     */
    public function getConfigurationActions()
    {
        return array();
    }
}



class articlemanager_Portlet_Articlemanager extends Widget_Item implements portlet_PortletInterface
{

    /**
     *
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * {@inheritDoc}
     * @see Widget_Widget::getName()
     */
    public function getName()
    {
        return get_class($this);
    }


    /**
     * {@inheritDoc}
     * @see portlet_PortletInterface::getPortletDefinition()
     */
    public function getPortletDefinition()
    {
        return new articlemanager_PortletDefinition_Articlemanager();
    }


    /**
     * {@inheritDoc}
     * @see portlet_PortletInterface::setPreferences()
     */
    public function setPreferences(array $configuration)
    {
        bab_Registry::override('/articlemanager/' . $this->id . '/displayType', $configuration['displayType']);
    }

    /**
     * {@inheritDoc}
     * @see Widget_Frame::display()
     */
    public function display(Widget_Canvas $canvas)
    {
        $App = articlemanager_App();
        $addon = bab_getAddonInfosInstance('articlemanager');

        $ctrl = $App->Controller()->Article(false);



        $box = $ctrl->filteredView(null, null, $this->id);
//        $box->setId('articlemanager_' . $this->id); // The widget item id.

        $display = $box->display($canvas);

        return $display;
    }



    /**
     * {@inheritDoc}
     * @see portlet_PortletInterface::setPreference()
     */
    public function setPreference($name, $value)
    {

    }


    /**
     * {@inheritDoc}
     * @see portlet_PortletInterface::setPortletId()
     */
    public function setPortletId($id)
    {
        $this->id = $id;
    }
}
