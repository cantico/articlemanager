<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2018 by CANTICO ({@link http://www.cantico.fr})
 */


require_once dirname(__FILE__) . '/functions.php';

$App = articlemanager_App();

$App->includeTraceableRecordSet();


/**
 *
  PRIMARY KEY  (id)
  UNIQUE KEY uuid (uuid)
 * @method articlemanager_Topic get()
 * @method articlemanager_Topic newRecord()
 * @method articlemanager_Topic[] select()
 * 
 * @param   ORM_IntField                    $id
 * @param   ORM_StringField                 $category
 * @param   ORM_TextField                   $description
 * @param   ORM_StringField                 $description_format
 * @param   ORM_IntField                    $idsaart
 * @param   ORM_IntField                    $idsacom
 * @param   ORM_IntField                    $idsa_update
 * @param   ORM_EnumField                   $notify
 * @param   ORM_StringField                 $lang
 * @param   ORM_StringField                 $article_tmpl
 * @param   ORM_StringField                 $display_tmpl
 * @param   ORM_EnumField                   $restrict_access
 * @param   ORM_EnumField                   $allow_hpages
 * @param   ORM_EnumField                   $allow_pupdates
 * @param   ORM_EnumField                   $allow_attachments
 * @param   ORM_EnumField                   $allow_update
 * @param   ORM_IntField                    $max_articles
 * @param   ORM_EnumField                   $allow_manupdate
 * @param   ORM_EnumField                   $auto_approbation
 * @param   ORM_EnumField                   $busetags
 * @param   ORM_EnumField                   $allow_addImg
 * @param   ORM_EnumField                   $allow_article_rating
 * @param   ORM_BoolField                   $allow_unsubscribe
 * @param   ORM_StringField                 $uuid
 * @param   ORM_StringField                 $allow_meta
 * @param   ORM_BoolField                   $allow_empty_head
 * @param   articlemanager_TopicCategory    $id_cat
 */
class articlemanager_TopicSet extends app_RecordSet
{
    /**
     * @param Func_App $App
     */
    public function __construct(Func_App $App = null)
    {
        parent::__construct($App);
        $this->setTableName(BAB_TOPICS_TBL);
        $App = $this->App();

        $this->setDescription($App->translatable('Topic'));

        $this->setPrimaryKey('id');

        $this->addFields(
            ORM_StringField('category')
                ->setDescription($App->translatable('Category')),
            ORM_TextField('description')
                ->setDescription($App->translatable('Description')),
            ORM_StringField('description_format')
                ->setDescription($App->translatable('Description format')),
            ORM_IntField('idsaart'),
            ORM_IntField('idsacom'),
            ORM_IntField('idsa_update'),
            ORM_EnumField('notify', array('N' => 'N', 'Y' => 'Y')),
            ORM_StringField('lang'),
            ORM_StringField('article_tmpl'),
            ORM_StringField('display_tmpl'),
            ORM_EnumField('restrict_access', array('N' => 'N', 'Y' => 'Y')),
            ORM_EnumField('allow_hpages', array('N' => 'N', 'Y' => 'Y')),
            ORM_EnumField('allow_pupdates', array('N' => 'N', 'Y' => 'Y')),
            ORM_EnumField('allow_attachments', array('N' => 'N', 'Y' => 'Y')),
            ORM_EnumField('allow_update', array('0' => '0', '1' => '1', '2' => '2')),
            ORM_IntField('max_articles'),
            ORM_EnumField('allow_manupdate', array('0' => '0', '1' => '1', '2' => '2')),
            ORM_EnumField('auto_approbation', array('N' => 'N', 'Y' => 'Y')),
            ORM_EnumField('busetags', array('N' => 'N', 'Y' => 'Y')),
            ORM_EnumField('allow_addImg', array('N' => 'N', 'Y' => 'Y')),
            ORM_EnumField('allow_article_rating', array('N' => 'N', 'Y' => 'Y')),
            ORM_BoolField('allow_unsubscribe'),
            ORM_StringField('uuid', 36),
            ORM_StringField('allow_meta'),
            ORM_BoolField('allow_empty_head')
        );

       $this->hasOne('id_cat', $App->TopicCategorySetClassName());
    }

    /**
     * {@inheritDoc}
     * @see app_RecordSet::isCreatable()
     */
    public function isCreatable()
    {
        return true;
    }


    /**
     * @return ORM_Criteria
     */
    public function isReadable()
    {
        return $this->all();
    }


    /**
     * @return ORM_Criteria
     */
    public function isUpdatable()
    {
        return $this->all();
    }

    /**
     * @return ORM_Criteria
     */
    public function isDeletable()
    {
        return $this->isUpdatable();
    }
}




/**
 * @param   int                             $id
 * @param   string                          $category
 * @param   string                          $description
 * @param   string                          $description_format
 * @param   int                             $idsaart
 * @param   int                             $idsacom
 * @param   int                             $idsa_update
 * @param   string                          $notify
 * @param   string                          $lang
 * @param   string                          $article_tmpl
 * @param   string                          $display_tmpl
 * @param   string                          $restrict_access
 * @param   string                          $allow_hpages
 * @param   string                          $allow_pupdates
 * @param   string                          $allow_attachments
 * @param   string                          $allow_update
 * @param   int                             $max_articles
 * @param   string                          $allow_manupdate
 * @param   string                          $auto_approbation
 * @param   string                          $busetags
 * @param   string                          $allow_addImg
 * @param   string                          $allow_article_rating
 * @param   bool                            $allow_unsubscribe
 * @param   string                          $uuid
 * @param   string                          $allow_meta
 * @param   bool                            $allow_empty_head
 * @param   articlemanager_TopicCategory    $id_cat
 */
class articlemanager_Topic extends app_TraceableRecord
{
}
