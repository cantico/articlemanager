<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */


$W = bab_Widgets();

$W->includePhpClass('Widget_Frame');
$W->includePhpClass('Widget_VBoxLayout');
$W->includePhpClass('Widget_TableModelView');


/**
 * list of articles
 *
 * @method Func_App_Articlemanager    App()
 */
class articlemanager_ArticleTableView extends app_TableModelView
{
    /**
     * @param Func_App $app
     * @param string $id
     */
    public function __construct(Func_App $app = null, $id = null)
    {
        parent::__construct($app, $id);
        $this->addClass('depends-articlemanager-article');
    }


    /**
     * @param app_CtrlRecord $recordController
     * @return directorymanager_DirectoryEntryTableView
     */
    public function setRecordController(app_CtrlRecord $recordController)
    {
        $this->recordController = $recordController;
        return $this;
    }

    /**
     * @return app_CtrlRecord
     */
    public function getRecordController()
    {
        return $this->recordController;
    }


    /**
     * {@inheritDoc}
     * @see widget_TableModelView::addDefaultColumns()
     */
    public function addDefaultColumns(ORM_RecordSet $recordSet)
    {
        $App = $this->App();

        $this->addColumn(
            app_TableModelViewColumn($recordSet->title)
        );
        $this->addColumn(
            app_TableModelViewColumn($recordSet->head)
        );
        $this->addColumn(
            app_TableModelViewColumn($recordSet->id_author)
        );
        $this->addColumn(
            app_TableModelViewColumn($recordSet->date)
        );
        $this->addColumn(
            app_TableModelViewColumn($recordSet->archive)
        );

        $this->addColumn(
            app_TableModelViewColumn('_actions_', ' ')
                ->setExportable(false)
                ->setSortable(false)
                ->addClass('widget-column-thin', 'widget-align-center')
        );
    }



//     /**
//      * {@inheritDoc}
//      * @see widget_TableModelView::handleRow()
//      */
//     protected function handleRow(ORM_Record $record, $row)
//     {
//         /* @var $record articlemanager_Article */
//         if ($record->isCompleted()) {
//             $this->addRowClass($row, 'article-completed');
//         } else {
//             if ($record->dueDate < date('Y-m-d')) {
//                 $this->addRowClass($row, 'article-late');
//             }
//         }

//         return parent::handleRow($record, $row);
//     }


    /**
     * @param ORM_Record    $record
     * @param string        $fieldPath
     * @return Widget_Item
     */
    protected function computeCellContent(ORM_Record $record, $fieldPath)
    {
        $W = bab_Widgets();
        $App = $this->App();

        /* @var $record articlemanager_Article */

        switch ($fieldPath) {
            case 'head':
                return $W->Html(nl2br(bab_abbr(bab_strip_tags($record->head), BAB_ABBR_FULL_WORDS, 100)));
                break;

            case 'id_author':
                return $W->Link(
                bab_getUserName($record->id_author, true)
            );


            case '_actions_':
                $box = $W->HBoxItems();
                if ($record->isUpdatable()) {
                    $box->setSizePolicy(Func_Icons::ICON_LEFT_SYMBOLIC);
                    $box->addItem(
                        $W->Link(
                            '', $this->App()->Controller()->Article()->edit($record->id)
                        )->addClass('icon', Func_Icons::ACTIONS_DOCUMENT_EDIT)
                        ->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD)
                    );
                }
                if ($record->isDeletable()) {
                    $box->addItem(
                        $W->Link(
                            '', $this->App()->Controller()->Article()->confirmDelete($record->id)
                        )->addClass('icon', Func_Icons::ACTIONS_EDIT_DELETE)
                        ->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD)
                    );
                }
                return $box;
        }

        return parent::computeCellContent($record, $fieldPath);
    }
}





class articlemanager_ArticleCardsView extends articlemanager_ArticleTableView
{

    /**
     *
     * @param Func_App $App
     * @param string $id
     */
    public function __construct(Func_App $App = null, $id = null)
    {
        parent::__construct($App, $id);

        $W = bab_Widgets();
        $layout = $W->FlowLayout();
        $layout->setVerticalAlign('top');
        $layout->setSpacing(1, 'em');
        $this->setLayout($layout);
    }

    /**
     * {@inheritDoc}
     * @see widget_TableModelView::initHeaderRow()
     */
    protected function initHeaderRow(ORM_RecordSet $set)
    {
        return;
    }

    /**
     * {@inheritDoc}
     * @see Widget_TableView::addSection()
     */
    public function addSection($id = null, $label = null, $class = null, $colspan = null)
    {
        return $this;
    }

    /**
     * {@inheritDoc}
     * @see Widget_TableView::setCurrentSection()
     */
    public function setCurrentSection($id)
    {
        return $this;
    }


    /**
     * {@inheritDoc}
     * @see widget_TableModelView::handleRow()
     */
    protected function handleRow(ORM_Record $record, $row)
    {
        $App = $this->App();
        $Ui = $App->Ui();
        $card = $Ui->ArticleCardFrame($record);
        $card->addClass('crm-card');
        $card->setSizePolicy('col-xs-12 col-sm-6 col-md-4 col-lg-3');
        $this->addItem($card);
        return true;
    }

    /**
     * {@inheritDoc}
     * @see widget_TableModelView::display()
     */
    public function display(Widget_Canvas $canvas)
    {
        $this->init();

        $items = array();

        $total = $this->handleTotalDisplay();
        $selector = $this->handlePageSelector();


        if (null !== $total) {
            $items[] = $total->display($canvas);
        }

        $list = parent::getLayout();
        $items[] = $list;

        if (null !== $selector) {
            $items[] = $selector->display($canvas);
        }

        $widgetsAddon = bab_getAddonInfosInstance('widgets');

        return $canvas->vbox(
            $this->getId(),
            $this->getClasses(),
            $items,
            $this->getCanvasOptions(),
            $this->getTitle(),
            $this->getAttributes()
        )
        . $canvas->metadata($this->getId(), $this->getMetadata())
        . $canvas->loadScript($this->getId(), $widgetsAddon->getTemplatePath() . 'widgets.tableview.jquery.js');
    }
}



class articlemanager_CardFrame extends app_UiObject
{
}


class articlemanager_ArticleFrame extends articlemanager_CardFrame
{
}


class articlemanager_ArticleFullFrame extends articlemanager_ArticleCardFrame
{
}


class articlemanager_ArticleCardFrame extends articlemanager_ArticleFrame
{
}





/**
 * @return app_Editor
 */
class articlemanager_ArticleEditor extends app_Editor
{
    /**
     * Add fields into form
     * @return crm_NoteEditor
     */
    public function prependFields()
    {
        return $this;
    }

    /**
     * Add fields into form
     * @return self
     */
    public function appendFields()
    {
        $W = bab_Widgets();

//        $this->addItem($this->topic());
        $this->addItem($this->title());
        $this->addItem($this->date());
        $this->addItem($this->head());
        $this->addItem($this->id_author());

        if (isset($this->record)) {
            $this->addItem(
                $W->Hidden()->setName('id')->setValue($this->record->id)
            );
        }

        return $this;
    }


    protected function topic()
    {
        $App = $this->App();

        $articleSet = $App->ArticleSet();

        $articleTopicSet = $App->ArticleTopicSet();

        $topics = $articleTopicSet->select();

        if ($topics->count() === 0) {
            return null;
        }

        return $this->labelledField(
            $App->translate('Topic'),
            app_OrmWidget($articleSet->id_topic),
            'id_topic'
        );
    }

    protected function title()
    {
        $W = bab_Widgets();
        $App = $this->App();

        return $this->labelledField(
            $App->translate('Title'),
            $W->LineEdit()
                ->setMandatory(true, $App->translate('The article title must not be empty.')),
            'title'
        );
    }

    protected function date()
    {
        $App = $this->App();

        return $this->labelledField(
            $App->translate('Date'),
            app_OrmWidget($this->getRecordSet()->date),
            'date'
        );
    }

    protected function head()
    {
        $App = $this->App();

        return $this->labelledField(
            $App->translate('Head'),
            app_OrmWidget($this->getRecordSet()->head),
            'head'
        );
    }

    protected function id_author()
    {
        $App = $this->App();

        return $this->labelledField(
            $App->translate('Author'),
            app_OrmWidget($this->getRecordSet()->id_author),
            'id_author'
        );
    }
}

