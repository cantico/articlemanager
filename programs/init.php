<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__) . '/functions.php';



function articlemanager_upgrade($version_base, $version_ini)
{
    $addonName = 'articlemanager';
    $addon = bab_getAddonInfosInstance($addonName);

    $addonPhpPath = $addon->getPhpPath();

    require_once $GLOBALS['babInstallPath'] . 'utilit/upgradeincl.php';
    require_once $GLOBALS['babInstallPath'] . 'utilit/functionalityincl.php';
    require_once $GLOBALS['babInstallPath'] . 'utilit/devtools.php';

    require_once dirname(__FILE__) . '/articlemanager.php';

    $functionalities = new bab_functionalities();

    if ($functionalities->registerClass('Func_App_Articlemanager', $addonPhpPath . 'articlemanager.php')) {
        echo(bab_toHtml('Functionality "Func_App_Articlemanager" registered.'));
    }

    $addon->removeAllEventListeners();

    $addonPhpPath = $addon->getPhpPath();

    $addon->addEventListener('bab_eventBeforeSiteMapCreated', 'articlemanager_onSiteMapItems', 'init.php');
    $addon->addEventListener('bab_eventBeforePageCreated', 'articlemanager_onBeforePageCreated', 'init.php', -10);

    @bab_functionality::includefile('PortletBackend');

    if (class_exists('Func_PortletBackend')) {
        require_once $GLOBALS['babInstallPath'].'utilit/functionalityincl.php';
        require_once dirname(__FILE__) . '/portletbackend.class.php';
        $functionalities = new bab_functionalities();
        $functionalities->registerClass('Func_PortletBackend_Articlemanager', $addonPhpPath . 'portletbackend.class.php');
    }

    @bab_functionality::includefile('WorkspaceAddon');
    if (class_exists('Func_WorkspaceAddon')) {
        $addonPhpPath = $addon->getPhpPath();
        require_once dirname(__FILE__) . '/workspaceaddon.class.php';
        $functionalities->registerClass('Func_WorkspaceAddon_Articlemanager', $addonPhpPath . 'workspaceaddon.class.php');
    }

    return true;
}



function articlemanager_onBeforePageCreated(bab_eventBeforePageCreated $event)
{
}



/**
 * Sitemap creation
 * @param bab_eventBeforeSiteMapCreated $event
 * @return mixed
 */
function articlemanager_onSiteMapItems(bab_eventBeforeSiteMapCreated $event)
{
    require_once dirname(__FILE__).'/functions.php';

    bab_functionality::includefile('Icons');

    $App = articlemanager_App();

    $mergeAddonsMenu = bab_Registry::get('/core/sitemap/mergeAddonsMenu', false);
    if ($mergeAddonsMenu) {
        $adminPosition = array('root', 'DGAll', 'babAdmin');
        $userPosition = array('root', 'DGAll', 'babUser');
    } else {
        $adminPosition = array('root', 'DGAll', 'babAdmin', 'babAdminSectionAddons');
        $userPosition = array('root', 'DGAll', 'babUser', 'babUserSectionAddons');
    }

    $item = $event->createItem('articlemanager_root');
    $item->setLabel($App->translate('Article manager'));
    $item->setLink($App->Controller()->Article()->displayList()->url());
    $item->setPosition($userPosition);
    $item->addIconClassname(Func_Icons::APPS_TASK_MANAGER);
    $event->addFolder($item);
}
