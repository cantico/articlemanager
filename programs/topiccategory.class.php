<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2018 by CANTICO ({@link http://www.cantico.fr})
 */


require_once dirname(__FILE__) . '/functions.php';

$App = articlemanager_App();

$App->includeTraceableRecordSet();


/**
 *
  PRIMARY KEY  (id)
  UNIQUE KEY uuid (uuid)
 * @method articlemanager_TopicCategory     get()
 * @method articlemanager_TopicCategory     newRecord()
 * @method articlemanager_TopicCategory[]   select()
 * 
 * @param   ORM_IntField        $id
 * @param   ORM_StringField     $title
 * @param   ORM_TextField       $description
 * @param   ORM_EnumField       $enabled
 * @param   ORM_StringField     $template
 * @param   ORM_IntField        $id_dgowner
 * @param   ORM_EnumField       $optional
 * @param   ORM_IntField        $id_parent
 * @param   ORM_StringField     $display_tmpl
 * @param   ORM_DatetimeField   $date_modification
 * @param   ORM_StringField     $uuid
 */
class articlemanager_TopicCategorySet extends app_RecordSet
{
    /**
     * @param Func_App $App
     */
    public function __construct(Func_App $App = null)
    {
        parent::__construct($App);
        $this->setTableName(BAB_TOPICS_CATEGORIES_TBL);
        $App = $this->App();

        $this->setDescription($App->translatable('Topic category'));

        $this->setPrimaryKey('id');

        $this->addFields(
            ORM_StringField('title')
                ->setDescription($App->translatable('Title')),
            ORM_TextField('description')
                ->setDescription($App->translatable('Description')),
            ORM_EnumField('enabled', array('N' => 'N', 'Y' => 'Y')),
            ORM_StringField('template'),
            ORM_IntField('id_dgowner'),
            ORM_EnumField('optional', array('N' => 'N', 'Y' => 'Y')),
            ORM_IntField('id_parent'),
            ORM_StringField('display_tmpl'),
            ORM_DatetimeField('date_modification'),
            ORM_StringField('uuid', 36)
        );
    }

    /**
     * {@inheritDoc}
     * @see app_RecordSet::isCreatable()
     */
    public function isCreatable()
    {
        return true;
    }


    /**
     * @return ORM_Criteria
     */
    public function isReadable()
    {
        return $this->all();
    }


    /**
     * @return ORM_Criteria
     */
    public function isUpdatable()
    {
        return $this->all();
    }

    /**
     * @return ORM_Criteria
     */
    public function isDeletable()
    {
        return $this->isUpdatable();
    }
}




/**
 * @param   int         $id
 * @param   string      $title
 * @param   string      $description
 * @param   string      $enabled
 * @param   string      $template
 * @param   int         $id_dgowner
 * @param   string      $optional
 * @param   int         $id_parent
 * @param   string      $display_tmpl
 * @param   datetime    $date_modification
 * @param   string      $uuid
 */
class articlemanager_TopicCategory extends app_TraceableRecord
{
}
