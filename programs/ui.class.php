<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */


require_once dirname(__FILE__) . '/functions.php';

$App = articlemanager_App();

$App->includeUi();

/**
 * The articlemanager_Ui class
 */
class articlemanager_Ui extends app_Ui
{
    /**
     * @param Func_App $app
     */
    public function __construct()
    {
        $this->setApp(articlemanager_App());
    }

    /**
     * Includes Article Ui helper functions definitions.
     */
    public function includeArticle()
    {
        require_once FUNC_ARTICLEMANAGER_UI_PATH . 'article.ui.php';
    }



    /**
     * Article table view
     * @return articlemanager_ArticleTableView
     */
    public function ArticleTableView()
    {
        $this->includeArticle();
        return new articlemanager_ArticleTableView($this->App());
    }


    /**
     * Article table view
     * @return articlemanager_ArticleCardsView
     */
    public function ArticleCardsView()
    {
        $this->includeArticle();
        return new articlemanager_ArticleCardsView($this->App());
    }


    /**
     * @param articlemanager_Article $article
     * @return articlemanager_ArticleCardFrame
     */
    public function ArticleCardFrame(articlemanager_Article $article)
    {
        $this->includeArticle();
        return new articlemanager_ArticleCardFrame($this->App(), $article);
    }


    /**
     * @param articlemanager_Article $article
     * @return articlemanager_ArticleFullFrame
     */
    public function ArticleFullFrame(articlemanager_Article $article)
    {
        $this->includeArticle();
        return new articlemanager_ArticleFullFrame($this->App(), $article);
    }


    /**
     * Article editor
     * @return articlemanager_ArticleEditor
     */
    public function ArticleEditor(articlemanager_Article $article = null)
    {
        $this->includeArticle();
        return new articlemanager_ArticleEditor($this->App(), $article);
    }
}
