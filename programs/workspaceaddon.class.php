<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2018 by CANTICO ({@link http://www.cantico.fr})
 */
require_once dirname(__FILE__).'/functions.php';

/**
 *
 */
class Func_WorkspaceAddon_Articlemanager extends Func_WorkspaceAddon implements workspace_Configurable
{
    /**
     * {@inheritDoc}
     * @see Func_WorkspaceAddon::getDescription()
     */
    public function getDescription()
    {
        return articlemanager_translate('Allow Worskpace addon to use Articlemanager');
    }


    /**
     * {@inheritDoc}
     * @see Func_WorkspaceAddon::getName()
     */
    public function getName()
    {
        return articlemanager_translate('Articles');
    }


    /**
     * {@inheritDoc}
     * @see Func_WorkspaceAddon::getConfigurationEditor()
     */
    public function getConfigurationEditor($workspaceId)
    {
        $W = bab_Widgets();

        $editor = parent::getConfigurationEditor($workspaceId);

        return $editor;
    }


    /**
     * {@inheritDoc}
     * @see Func_WorkspaceAddon::getIconClassName()
     */
    public function getIconClassName()
    {
        return Func_Icons::APPS_TASK_MANAGER;
    }


    /**
     * {@inheritDoc}
     * @see workspace_Configurable::applyConfiguration()
     */
    public function applyConfiguration($workspaceId)
    {
        /* @var $workspaceApi Func_App_Workspace */
        $workspaceApi = bab_Functionality::get('App/Workspace');
        if ($workspaceApi) {
            $workspaceSet = $workspaceApi->WorkspaceSet();
            /* @var $workspace workspace_Workspace */
            $workspace = $workspaceSet->request($workspaceSet->id->is($workspaceId));
            if ($workspace->delegation == 0 || $workspace->name == '') {
                return;
            }
            $configuration = $this->getConfiguration($workspace->id);
        }
    }


    /**
     * {@inheritDoc}
     * @see Func_WorkspaceAddon::getPortletDefinitionId()
     */
    public function getPortletDefinitionId()
    {
        /* @var $portletFunc Func_PortletBackend_Articlemanager */
        $portletFunc = bab_Functionality::get('PortletBackend/Articlemanager');
        if ($portletFunc) {
            /* @var $portlet articlemanager_PortletDefinition_Articlemanager */
            $portlet = $portletFunc->portlet_Articlemanager();
            return $portlet->getId();
        }
        return false;
    }


    /**
     * {@inheritDoc}
     * @see Func_WorkspaceAddon::getPortletConfiguration()
     */
    public function getPortletConfiguration($workspaceId)
    {
        $portletConfiguration = array(
            '_blockTitleType' => 'custom',
            '_blockTitle' => '',
            'displayType' => 'details'
        );
        /* @var $workspaceApi Func_App_Workspace */
        $workspaceApi = bab_Functionality::get('App/Workspace');
        if ($workspaceApi) {
            $workspaceSet = $workspaceApi->WorkspaceSet();
            /* @var $workspace workspace_Workspace */
            $workspace = $workspaceSet->request($workspaceSet->id->is($workspaceId));
            if ($workspace->delegation == 0 || $workspace->name == '') {
                return $portletConfiguration;
            }
        }
        return $portletConfiguration;
    }
}
