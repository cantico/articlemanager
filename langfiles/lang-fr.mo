��          �      �           	          !     )     9     H     Q     X     ]     b     v     �     �     �     �     �     �     �  $   �            &     �  >                    '     ?     W     `     g     m     r     �     �     �     �     �     �     �     �  -        6     <      C                                   
                                    	                                 Archived Archiving date Article Article manager Articlemanager Articles Author Body Date Delete this article Display type Duplicate this article Edit this article Head List Modification date Portal tools Publication date The article title must not be empty. Title Topic We have a problem finding this page... Project-Id-Version: articlemanager
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2019-02-18 15:43+0100
PO-Revision-Date: 2019-02-18 15:43+0100
Last-Translator: Laurent Choulette <laurent.choulette@cantico.fr>
Language-Team: Cantico <laurent.choulette@cantico.fr>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-Basepath: ..
X-Poedit-KeywordsList: articlemanager_translate;articlemanager_translate:1,2;translate;translate:1,2;translatable;translatable:1,2;articlemanager_translatable;articlemanager_translatable:1,2
X-Generator: Poedit 1.8.7.1
X-Poedit-SearchPath-0: programs
 Archivé Date d'archivage Article Gestionnaire d'articles Gestionnaire d'articles Articles Auteur Corps Date Supprimer cet article Type d'affichage Dupliquer cet article Modifier l'article Entête Liste Date de modification Outils portail Date de publication Le titre de l'article ne doit pas être vide. Titre Thème Impossible de trouver la page... 